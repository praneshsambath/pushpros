let express = require('express'),
    app = express(),
    path = require("path"),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    port = 8080;

app.use(cookieParser());
app.use(bodyParser.json());

require("./routes")(app);


app.use("/", express.static(path.join(__dirname, "../build")));

app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, "../build", "index.html"));
});

app.set('port', port);

// Start Server
app.listen(port, () => {
    console.log("Running on port " + port);
});

