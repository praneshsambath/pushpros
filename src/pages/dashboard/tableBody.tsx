import React from "react";
import { TableBodyProps, TableResponse } from "../../common/interface/index";

const TableBody: Function = ({
  tableData,
}: TableBodyProps): JSX.Element[] => {
  return tableData?.length > 0 ? (
    tableData.map((item: TableResponse) => (
      <tr key={item.id}>
        <th scope="row">{item.id}</th>
        <td>{item.name}</td>
        <td>{item.url}</td>
      </tr>
    ))
  ) : (
    <td colSpan={3} className="no-data">
      No Data
    </td>
  );
};

export default TableBody;
