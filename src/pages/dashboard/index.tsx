import React from "react";
import { connect } from "react-redux";
import Button from "../../common/button";
import { DashboardProps } from "../../common/interface/index";
import Loader from "../../common/loader";
import Table from "../../common/table";
import { loadTableData } from "../../redux/dashboard/action";
import { AppState } from "../../redux/store";
import "./index.scss";
import TableBody from "./tableBody";
import TableHeader from "./tableHeader";

const Dashboard = React.memo(
  ({ loadTableData, isLoading, tableData }: DashboardProps) => {
    let headerName: string[] = ["id", "name", "url"];

    const header = <TableHeader headerName={headerName} />;

    const body = isLoading ? (
      <Loader className={"loader"} />
    ) : (
      <TableBody tableData={tableData} />
    );

    const loadData = () => {
      loadTableData();
    };

    return (
      <div className="dashboard">
        <div className="action-btn">
          <Button
            type={"button"}
            className={"load-data-btn"}
            onClick={() => loadData()}
          >
            Load Data
          </Button>
        </div>
        <div className="parent-table">
          <Table header={header} body={body} tableType={"table-bordered"} />
        </div>
      </div>
    );
  }
);
const mapStateToProps = (state: AppState) => {
  const { tableData, isLoading } = state.DashboardReducer;
  return { tableData, isLoading };
};

const mapDispatchToProps = {
  loadTableData: () => loadTableData(),
};
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
