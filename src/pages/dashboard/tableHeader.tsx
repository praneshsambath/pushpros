import React from "react";
import { TableHeaderProps } from "../../common/interface/index";

const TableHeader: Function = (props: TableHeaderProps): JSX.Element[] => {
  let { headerName } = props;

  return headerName.map((item: string, index: number) => (
    <th scope="col" key={index}>
      {item}
    </th>
  ));
};

export default TableHeader;
