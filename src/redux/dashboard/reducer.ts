import { AnyAction } from "redux";
import { actionTypes } from "../actionTypes";
import { DashBoardState } from "../../common/interface";

export const initialState: DashBoardState = {
  tableData: [],
  isLoading: false,
};

export const DashboardReducer = (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case actionTypes.LOADER: {
      return {
        ...state,
        isLoading: action.payload,
      };
    }
    case actionTypes.LOAD_DATA: {
      return {
        ...state,
        tableData: action.payload,
      };
    }
    default:
      return state;
  }
};
