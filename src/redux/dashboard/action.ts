import { AnyAction } from "redux";
import { ThunkDispatch } from "redux-thunk";
import { TableResponse } from "../../common/interface";
import httpClient from "../../service/httpClient";
import { AppState } from "../store";
import { actionTypes } from "../actionTypes";
import { api } from "../../service/api";

export const loadTableData = () => (
  dispatch: ThunkDispatch<AppState, {}, AnyAction>
) => {
  dispatch({ type: actionTypes.LOADER, payload: true });
  const onApiSuccess = (payload: TableResponse) => {
    dispatch({ type: actionTypes.LOAD_DATA, payload });
    dispatch({ type: actionTypes.LOADER, payload: false });
  };
  const onError = (err: any) => {
    dispatch({ type: actionTypes.LOADER, payload: false });
  };
  httpClient
    .getInstance()
    .get(api.loadTable)
    .then((response) => response.data.sites)
    .then((payload: TableResponse) => onApiSuccess(payload))
    .catch((err) => onError(err));
};
