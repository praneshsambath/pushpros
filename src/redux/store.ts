import { applyMiddleware, combineReducers, createStore } from "redux";
import { createLogger } from "redux-logger";
import thunk from "redux-thunk";
import { DashboardReducer } from "./dashboard/reducer";

export const rootReducer = combineReducers({
  DashboardReducer,
});

const allMiddlewares = [];
if (process.env.NODE_ENV !== "production") {
  const logger = createLogger({
    collapsed: true,
  });
  allMiddlewares.push(logger);
}

export type AppState = ReturnType<typeof rootReducer>;

export const store = createStore(
  rootReducer,
  applyMiddleware(thunk, ...allMiddlewares)
);
