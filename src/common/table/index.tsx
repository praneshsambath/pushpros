import React from "react";
import { TableProps } from "../interface/index";

export default React.memo(({ header, body, tableType }: TableProps) => {
  return (
    <table className={`table ${tableType}`} >
      <thead>
        <tr>{header}</tr>
      </thead>
      <tbody>{body}</tbody>
    </table>
  );
});
