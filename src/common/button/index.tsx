import React from "react";
import { IButton } from "../interface/index";

export default React.memo(
  ({
    type,
    className,
    children,
    onClick,
    disabled,
    dataToggle,
    ariaHaspopup,
    ariaExpanded,
    dataTarget,
    dataDismiss,
    ariaLabel,
  }: IButton) => {
    return (
      <React.Fragment>
        <button
          type={type}
          className={className}
          data-toggle={dataToggle}
          aria-haspopup={ariaHaspopup}
          aria-expanded={ariaExpanded}
          data-target={dataTarget}
          aria-label={ariaLabel}
          data-dismiss={dataDismiss}
          disabled={disabled}
          onClick={onClick}
        >
          {children}
        </button>
      </React.Fragment>
    );
  }
);
