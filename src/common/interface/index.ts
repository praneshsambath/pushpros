export interface TableProps {
  header: React.ReactNode;
  body: React.ReactNode;
  tableType?: string;
}
export interface IButton {
  type: "button" | "submit" | "reset";
  className: string;
  children?: React.ReactNode;
  onClick?: (e: React.MouseEvent) => void;
  disabled?: boolean;
  dataToggle?: string;
  ariaHaspopup?: boolean;
  ariaExpanded?: boolean;
  dataTarget?: string;
  dataDismiss?: string;
  ariaLabel?: string;
}
export interface DashboardProps {
  loadTableData: () => void;
  tableData: TableResponse[];
  isLoading: boolean;
}
export interface TableBodyProps {
  tableData: TableResponse[] | any;
}
export interface TableResponse {
  id: number;
  uid: string;
  url: string;
  name: string;
  icon: string;
  promptId: number;
  webPushID: string;
  auto_prompt: number;
  createdAt: string;
  active: number;
}

export interface DashBoardState {
  tableData: TableResponse[];
  isLoading: boolean;
}

export interface TableHeaderProps {
  headerName: string[];
}
export interface LoaderProps {
  className: string;
}
