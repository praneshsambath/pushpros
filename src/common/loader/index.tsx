import React from "react";
import { LoaderProps } from "../interface";
import "./index.scss";

export default React.memo(({ className }: LoaderProps) => {
  return (
    <div className={`${className} spinner-border`} role="status">
      <span className="visually-hidden"></span>
    </div>
  );
});
